import React from 'react';
import TopMenu from './components/TopMenu.js';
import Sidebar from './components/Sidebar.js';
import Main from './components/Main.js';
import './styles/App.css';

/*The page is divided in 3 main components:
	-TopMenu: The fixed header of the page, contains the page icon, 
			  the selectable options (Overview, Campaigns, Analytics) and the premium button.
	-Sidebar: This option bar is located on the left side of the page, but it transforms into a secondary header 
			  when viewed from mobile resolutions. The option icons are selectable.
	-Main: Contains the actual information, contains the graphs, term info and the support request table.
		   These subcomponents have their own class for reusability.
*/
function App() {
  return (
    <div className="App" >
      <TopMenu /> {/*This element isn't inside the appContainer since it's always at the top of the page*/}
      <div className="appContainer">
        <Sidebar />
        <Main />
      </div>
    </div>
  );
}

export default App;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST