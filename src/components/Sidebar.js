import React from 'react';
import '../styles/Sidebar.css';

class Sidebar extends React.Component {
    constructor(props) {
        super(props);

        this.toggleActiveLink = this.toggleActiveLink.bind(this);
        this.state = {activeIndex: 0}; //Used to toggle the active option in the sidebar, active classes are styled differently
    }

    //Called on sidebar option click, receives the element index to change 'activeIndex' state
    toggleActiveLink(index){
      const currentIndex = index;
      this.setState({ activeIndex: index });
    }

	render() {
	  return (
	    <div className="Sidebar">
	      <div className="sidebarItemContainer">
	        <a className={this.state.activeIndex==0 ? 'sidebarItemOption active': 'sidebarItemOption'}  onClick={this.toggleActiveLink.bind(this, 0)}><i className="fas fa-tachometer-alt"></i></a>
	        <a className={this.state.activeIndex==1 ? 'sidebarItemOption active': 'sidebarItemOption'}  onClick={this.toggleActiveLink.bind(this, 1)}><i className="fas fa-comments"></i></a>
	        <a className={this.state.activeIndex==2 ? 'sidebarItemOption active': 'sidebarItemOption'}  onClick={this.toggleActiveLink.bind(this, 2)}><i className="fas fa-folder-open"></i></a>
	        <a className={this.state.activeIndex==3 ? 'sidebarItemOption active': 'sidebarItemOption'}  onClick={this.toggleActiveLink.bind(this, 3)}><i className="fas fa-envelope"></i></a>
	        <a className={this.state.activeIndex==4 ? 'sidebarItemOption active': 'sidebarItemOption'}  onClick={this.toggleActiveLink.bind(this, 4)}><i className="fas fa-cog"></i></a>
	      </div>
	    </div>
	  );
	}
}

export default Sidebar;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST