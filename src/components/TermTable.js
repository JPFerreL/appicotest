import React from 'react';
import '../styles/TermTable.css';

class TermTable extends React.Component {
    constructor(props) {
        super(props);
        /*The property received is the object for mapping the key-value pairs easily*/
    }

    render() {
      {/*Mapping for the term values, key is used to create the corresponding label (TERM 1, TERM 2, etc...)*/}
      const termList = Object.entries(this.props.values).map(([key,value])=>{
        return (
            <div className="term" key={key}>
              <div className="termKey">TERM {key} </div>
              <div className="termValue">{value.toString()}</div>
            </div>
        );
      });
    return (
      <div className="TermTable">
        <div className="termContainer">
      	 {termList}
        </div>
      </div>
    );
  }
}

export default TermTable;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST