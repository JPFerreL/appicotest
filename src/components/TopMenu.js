import React from 'react';
import '../styles/TopMenu.css';

class TopMenu extends React.Component {
    constructor(props) {
        super(props);

        this.toggleActiveLink = this.toggleActiveLink.bind(this);
        this.state = {activeIndex: 0}; //Used to toggle the active option in the header, active classes are styled differently
    }

    //Called on header option click, receives the element index to change 'activeIndex' state
    toggleActiveLink(index){
      const currentIndex = index;
      this.setState({ activeIndex: index });
    }

  render() {
    return (
      <div className="TopMenu">
        <div className="iconContainer">
          <span className="fa-stack"> {/*Stackable variants of FontAwesome icons to be able to overlay text on the page icon*/}
            <i className="fa fa-circle fa-stack-2x circleIcon"></i>
            <strong className="fa-stack-1x fa-stack-text fa-inverse dotSpan">.</strong>
          </span>
        </div>
        <div className="menuItemContainer">
          <a className={this.state.activeIndex==0 ? 'menuItemOption active': 'menuItemOption'}  onClick={this.toggleActiveLink.bind(this, 0)}>Overview</a>
          <a className={this.state.activeIndex==1 ? 'menuItemOption active': 'menuItemOption'}  onClick={this.toggleActiveLink.bind(this, 1)}>Campaigns</a>
          <a className={this.state.activeIndex==2 ? 'menuItemOption active': 'menuItemOption'}  onClick={this.toggleActiveLink.bind(this, 2)}>Analytics</a>
        </div>
        <div className="premiumButtonContainer">
          <button type="button" className="premiumButton">Premium</button>
        </div>
      </div>
    );
  }
}

export default TopMenu;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST