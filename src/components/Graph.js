import React from 'react';
import '../styles/Graph.css';
import DonutChart from 'react-donut-chart';
/*
'react-donut-chart' is an external package for react to implement very simple donut charts, I could have used other packages such as 'Canvas.js' or 'Charts.js'
but for this example project a lighter package is best. 
I have decided to use this package since the only way I can think of for representing pie/donut charts without external libraries is with HTML SVG, and it would 
be too complicated to create by myself.
*/

class Graph extends React.Component {
    constructor(props) {
        super(props);
        /* Props received are:
          -followers: Number of followers for the charts
          -colorScheme: Used to determine classes for the styling of the charts
        */

        this.state={barGraphData:'abcdefghijklmno'.split('')} //Simple array for the bar graph labels and columns
    }

    //Random function for the height of the bar chart entries
    randomBarHeight(min, max) {
  		return Math.floor(Math.random() * (max - min + 1) + min) + "%";
	  }

    render() {
      {/*Mapping of the bar chart values, assigns a random height(value) to each entry and an alternating color scheme*/}
      const barGraph = this.state.barGraphData.map((item, index)=>{
      const randomHeight = { "height" : this.randomBarHeight(10, 70) }
        return (
            <div className="barItem" key={index}>
              <div className={` ${this.props.colorScheme}  ${index%2==0?"bar":"bar altColor"}`} style={randomHeight}>
              	&nbsp;
              </div>
              {item}
            </div>
        );
      });

      {/*DonutChart component, for the attributes refer to the API documentation referenced in the README of this project*/}
      const donutGraph= <DonutChart className="donutChart" legend={false} width={200} height={200} clickToggle={false} strokeColor="none" startAngle={120}
        data={[{
            label: '',
            value: 40,
            className:this.props.colorScheme,
        },
        {
            label: '',
            value: 60,
            className:this.props.colorScheme + " altColor",
        }]} />

    return (
      <div className="Graph">
      	<div className="mainGraphContainer">
      		<div className="graphTitle">{this.props.title}</div>
      		<div className="graphContainer">
      			<div className="graphContainer1">
	      			<div>
	      				<span className="followerNumber">{this.props.followers.toLocaleString('de-DE')}</span> {/*toLocaleString used for thousand separator*/}
	      				<span className="followerText"> Followers</span>
	      			</div>
	      			<div className="barGraphContainer">
	      				{barGraph}
	      			</div>
	      		</div>
	      		<div className="graphContainer2">
              {donutGraph}
	      		</div>
      		</div>
      	</div>
      </div>
    );
  }
}

export default Graph;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST