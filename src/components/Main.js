import React, { useState } from 'react';
import '../styles/Main.css';
import Graph from './Graph.js';
import TermTable from './TermTable.js';
import SupportRequests from './SupportRequests.js';
/*
Imports for the sub-components of the main page:
  -Graph: Corresponding to the Bar and Donut chart block. Details inside Graph.js file.
  -TermTable: Corresponding to the term information block. Details inside TermTable.js file.
  -SupportRequests: Corresponding to the Support requests block, containing the table. Details inside SupportRequests.js file.

Sub-components for the progress bar at the top and the page selector are created as functions inside Main component.
*/


class Main extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        	termValues:{
        		"1": 85.08,
        		"2": 1.76,
        		"3": 33.42,
        		"4": 75.11
        	}
        }; //TermValues is created as an object for mapping the key-value pairs easily
    }

  render() {
    return (
      <div className="Main">
      	<div className="titleContainer">
      		<Title text="Data overview" /> {/*Title of the current view, could be created as a state in a superior component and passed to Main if more pages were available in this example*/}
			    <ProgressBar />
      	</div>
      	<div className="infoContainer">
      		<Graph title="General results" followers={9401} colorScheme="colorScheme1"/> {/*Two color schemes avaiable, blue and red as per the example given for this project*/}
      		<Graph title="Ratings by category" followers={3900} colorScheme="colorScheme2"/>
      		<TermTable values={this.state.termValues}/>
      		<SupportRequests />
      	</div>
      	<PageIndexContainer elementsPerPage={20} totalElements={25} /> {/*Change these values to add more pages in the page index the bottom of the page*/}
      </div>
    );
  }
}

/*Elements conforming the top section of the main page: The Title and the Progress Bar*/
function Title(props){

	return (
      <div className="titleText">
      	{props.text}
      </div>
    );
}

function ProgressBar(){
	const [progressValue, setProgressValue] = useState(1357); /*Modify these hooks to change the progress bar meter*/
	const [progressMax, setProgressMax] = useState(2000);

	return (
      <div className="insertionContainer">
        <progress className="insertionBar" value={progressValue} max={progressMax}></progress>
		    <label className="insertionInfo"><span className="insertionNumber">{progressMax - progressValue}</span> <span className="insertionText">insertions needed to complete</span></label>
      </div>
    );
}


/*Elements conforming the bottom section of the main page: Page index visualizer and page selector (working!)*/
function PageIndexContainer(props){
	const [visiblePage, setVisiblePage] = useState(1); /*Modify this hook to highlight a different page index*/

	var totalPages = Math.ceil(props.totalElements/props.elementsPerPage); //Calculating total pages
	let pages = []; //This array will contain every selectable page in the bottom page index, filled in the next for loop.
	for(let i=1;i<=totalPages;i++){
		if(i==totalPages){ //Add the three-dot separator before the last page
			pages.push(<div key="separator" className="pageSeparator">...</div>);
		}
		pages.push(<div key={i} className={visiblePage==i ? 'pageOption active': 'pageOption'} onClick={toggleActivePage.bind(this, i)}>{i}</div>);
	}

  //Called on page index option click, receives the element index to change the highlighted active page
	function toggleActivePage(index){
      const currentPage = index;
      setVisiblePage(currentPage);
    }

	return (
      <div className="pageIndexContainer">
      	<div className="pageInfo"> {/*The math and logic operators inside determines the values tho show on the current page index information*/}
	      	Showing <span className="strongPageInfo">{props.elementsPerPage*visiblePage - (props.elementsPerPage-1)}</span> to
	      	<span className="strongPageInfo"> {props.elementsPerPage*visiblePage < props.totalElements?props.elementsPerPage*visiblePage:props.totalElements} </span>
	      	of {props.totalElements} elements
     	  </div>
       	<div className="pageSelector"> {/*Working selectors*/}
       		Page <div className="pageOptionContainer">{pages}</div>
        
       	</div>
      </div>
    );
}

export default Main;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST