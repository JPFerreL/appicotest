import React from 'react';
import '../styles/SupportRequests.css';

class SupportRequests extends React.Component {
    constructor(props) {
        super(props);

        this.state = {users: []}; //User data will be fetched from a dummy API online
    }

    /*Fetch data from api when component is finished mounting, slice the array to get an appropiate number of elements*/
    componentDidMount() {
    	fetch('https://jsonplaceholder.typicode.com/users')
		.then(response => response.json())
		.then(json => this.setState({users:json.slice(0,6)}));
    }

    render() {
    	var time = new Date(); {/*Used to provide a value to the 'TIME' column in the table, since the API doesn't have a suitable value for this*/}
    	{/*Mapping of the user array into rows for the table, current time is provided for the 'Time' column and a regular button for the 'Status' column.
    	  Buttons have a different class after the fourth column to show a different styling*/}
    	const tableRows = Object.entries(this.state.users).map(([index,user])=>{
        return (
        	<tr key={index} className="userRow">
        		<td className="userNameCell">{user.name}</td>
        		<td>{user.email}</td>
        		<td>{time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}</td>
        		<td>{user.phone}</td>
        		<td>{user.address.city}</td>
        		<td><button type="button" className={index<4?'tableButton sentRequest':'tableButton openRequest'}>{index<4?'Sent':'Open'}</button></td>
        	</tr>

        );
      });
    return (
      <div className="SupportRequests">
      	<div className="supportRequestsTitle">Support Requests</div>
      	<div className="supportRequestsTableContainer">
      		<table className="supportRequestsTable">
      			<thead>
	      			<tr className="headerRow">
	      				<td>NAME</td>
	      				<td>EMAIL</td>
	      				<td>TIME</td>
	      				<td>PHONE NUMBER</td>
	      				<td>CITY</td>
	      				<td>STATUS</td>
	      			</tr>
      			</thead>
      			<tbody>
      				{tableRows}
      			</tbody>
      		</table>
      	</div>
      </div>
    );
  }
}

export default SupportRequests;

//JOSE PABLO FERRE LEORINI, FEBRUARY 2020 FOR APPICO FRONTEND DEVELOPER TEST