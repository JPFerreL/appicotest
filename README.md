### Developed by Jose Pablo Ferre Leorini on February 2020 for Appico Frontend Developer Test

## Frameworks and packages

I have created this test project using React.js.  
I have also used `react-donut-chart` for the donut graphs, since I considered HTML SVG to be very tedious to implement for an example task,
you can download the package from [here](https://www.npmjs.com/package/react-donut-chart) and also view the packages [API reference](https://github.com/vonbearshark/react-donut-chart).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Styling and responsive aspect

No external CSS libraries were used for this project.

I implemented breakpoints via media-querys to make my design responsive.  
I've chosen 'iPad' for my tablet resolution reference and 'Galaxy S5' as my mobile resolution reference, these were chosen since they are defaults in the 
Google Chrome device toolbar, you can find this by pressing `F12` with the project open in your browser and then pressing `Ctrl+Shift+M`, then select the device.

## Extras

I used the [provided dummy API](https://jsonplaceholder.typicode.com/) to fetch the user information for the 'Support Requests" table.

## How to start this test project:

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
